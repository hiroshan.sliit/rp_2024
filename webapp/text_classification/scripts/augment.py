import nltk
nltk.download('stopwords')
import random
import numpy as np
from text_classification.scripts.preprocess import tokenize_sent

class SSwap():
    """
    Saliency-based Token Swap (SSwap) class that performs token swapping between sentences
    based on their saliency values for data augmentation in text classification tasks.

    Methods
    -------
    swap_words(sent1, sent2, shap_1, shap_2, thresh, p):
        Swaps non-salient tokens between two sentences based on their SHAP values and a threshold.
    
    swap_imp_words(sent1, sent2, shap_1, shap_2, thresh, p):
        Swaps important (salient) tokens between two sentences based on their SHAP values and a threshold.
    
    bisent_swap(sent, rand_sent, shap, rand_shap, thresh, same_label=None, p=0.1):
        A high-level method that applies the appropriate token swapping strategy (either non-salient or
        salient token swapping) between two sentences depending on whether they share the same label.
    """
    def swap_words(self, sent1, sent2, shap_1, shap_2, thresh, p):
        """
        Swaps non-salient tokens between two sentences.

        Parameters
        ----------
        sent1 : str
            The first sentence.
        sent2 : str
            The second sentence.
        shap_1 : list of floats
            SHAP values for tokens in the first sentence.
        shap_2 : list of floats
            SHAP values for tokens in the second sentence.
        thresh : float
            Percentile threshold for determining which tokens are salient.
        p : float
            The proportion of non-salient tokens to swap between the sentences.

        Returns
        -------
        tuple of str
            The modified sentences after non-salient token swapping.
        """
        words1 = tokenize_sent(sent1)
        words2 = tokenize_sent(sent2)
        important_words_1 = [w for i, w in enumerate(words1) if shap_1[i] >= np.percentile(shap_1, thresh)]
        important_words_2 = [w for i, w in enumerate(words2) if shap_2[i] >= np.percentile(shap_2, thresh)]
        swappable_idx_1_ns = [idx for idx, w in enumerate(words1) if w not in important_words_1]
        swappable_idx_2_ns = [idx for idx, w in enumerate(words2) if w not in important_words_2]
        n = int(p * max(1, max(len(swappable_idx_1_ns), len(swappable_idx_2_ns))))
        for _ in range(n):
            if (len(swappable_idx_1_ns) > 0) and (len(swappable_idx_2_ns) > 0):
                swap_idx_1, swap_idx_2 = random.choice(swappable_idx_1_ns), random.choice(swappable_idx_2_ns)
                words1[swap_idx_1], words2[swap_idx_2] = words2[swap_idx_2], words1[swap_idx_1]
        return (' '.join(words1), ' '.join(words2))

    def swap_imp_words(self, sent1, sent2, shap_1, shap_2 ,thresh, p):
        words1 = tokenize_sent(sent1)
        words2 = tokenize_sent(sent2)
        important_words_1 = [w for i, w in enumerate(words1) if shap_1[i] >= np.percentile(shap_1, thresh)]
        important_words_2 = [w for i, w in enumerate(words2) if shap_2[i] >= np.percentile(shap_2, thresh)]
        swappable_idx_1 = [idx for idx, w in enumerate(words1) if w in important_words_1]
        swappable_idx_2 = [idx for idx, w in enumerate(words2) if w in important_words_2]
        swappable_idx_1_ns = [idx for idx, w in enumerate(words1) if w not in important_words_1]
        swappable_idx_2_ns = [idx for idx, w in enumerate(words2) if w not in important_words_2]
        
        n = int(p * max(1, min(len(words1), len(words2))))
        for _ in range(n):
            if (len(swappable_idx_1) > 0) and (len(swappable_idx_2) > 0):
                swap_idx_1, swap_idx_2 = random.choice(swappable_idx_1), random.choice(swappable_idx_2)
                words1[swap_idx_1], words2[swap_idx_2] = words2[swap_idx_2], words1[swap_idx_1]
            if (len(swappable_idx_1_ns) > 0) and (len(swappable_idx_2_ns) > 0):
                swap_idx_1, swap_idx_2 = random.choice(swappable_idx_1_ns), random.choice(swappable_idx_2_ns)
                words1[swap_idx_1], words2[swap_idx_2] = words2[swap_idx_2], words1[swap_idx_1]
        return (' '.join(words1), ' '.join(words2))

    def bisent_swap(self, sent, rand_sent, shap, rand_shap, thresh, same_label=None, p=.1):
        if not same_label:
            sent1, sent2 = self.swap_words(
                sent,
                rand_sent,
                shap,
                rand_shap, thresh, p
            )
        else:
            sent1, sent2= self.swap_imp_words(
                sent,
                rand_sent,
                shap,
                rand_shap, thresh, p
            )
        return tokenize_sent(sent1)

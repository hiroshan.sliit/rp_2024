from flask import Flask, render_template, request, session, redirect
import pandas as pd
from time import time
import numpy as np
# text-clf
from text_classification.scripts.evaluate import make_prediction
from text_classification.scripts.shap import get_shap_values_as_list
from text_classification.scripts.augment import SSwap
# image-clf
from flask import Flask, render_template, Response, request, redirect, url_for
import cv2
import numpy as np
from tensorflow.keras.models import model_from_json
from mtcnn import MTCNN
import shap
import os
from camvideo import *

app = Flask(__name__)
app.secret_key = 'shhhh_secret_key'
app.config['UPLOAD_FOLDER'] = 'video_uploads'

###########################################################################################3
# SIGNUP
from flask import Flask, render_template, request, redirect, url_for, flash
from flask_sqlalchemy import SQLAlchemy
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin, login_user, login_required, logout_user, current_user
from collections import Counter


app.config['SQLALCHEMY_DATABASE_URI'] = 'mysql://root:09821@localhost/my_recommender'
db = SQLAlchemy(app)
print("[DB] CONNECTION ESTABLISHED")

login_manager = LoginManager()
login_manager.init_app(app)
login_manager.login_view = 'signin'

class User(db.Model, UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    email = db.Column(db.String(100), unique=True)
    password = db.Column(db.String(200))  # Storing the hashed password
    age = db.Column(db.Integer)
    sex = db.Column(db.String(1))
    location = db.Column(db.String(50))
    relationship_status = db.Column(db.String(50))
    designation = db.Column(db.String(50))
    salary = db.Column(db.Integer)
    likes = db.Column(db.String(50))
    dislikes = db.Column(db.String(50))
    strengths = db.Column(db.String(50))
    weaknesses = db.Column(db.String(50))
    reason = db.Column(db.String(50))
    suggestion = db.Column(db.String(50))
    # is_active = True

@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

with app.app_context():
    db.create_all()


def get_max_voted_emotion(emotions):
    valid_emotions = [emotion for emotion in emotions if emotion is not None]
    if not valid_emotions:
        return None
    emotion_counts = Counter(valid_emotions)
    emotion = emotion_counts.most_common(1)[0][0]
    emot = ''
    if 'sad' in emotion.strip().lower():
        emot = 'Sadness'
    elif ('joy' in emotion.strip().lower()) or ('happy' in emotion.strip().lower()):
        emot = 'Joy'
    elif ('anger' in emotion.strip().lower()) or ('angry' in emotion.strip().lower()):
        emot = 'Anger'
    return emot

@app.route('/')
@login_required
def home():
    return render_template('index.html', 
                           text_clf=session.get('text_clf_emotion'), 
                           image_clf=session.get('image_clf_emotion'),
                           audio_clf=session.get('audio_clf_emotion'),
                           common=get_max_voted_emotion([session.get('text_clf_emotion'),
                                                         session.get('image_clf_emotion'),
                                                         session.get('audio_clf_emotion')]))
    
@app.route('/user_info')
@login_required
def user_info():
    return render_template('user_info.html', user=current_user)

@app.route('/clear')
@login_required
def clear():
    session.clear()
    return render_template('index.html')
#########################################################################
#  ******  TEXT-CLASSIFICATION   ******  ################################
#########################################################################
@app.route('/text_classification', methods=['GET', 'POST'])
@login_required
def text_classification():
    model = session.get('text_clf_model')
    lang = session.get('lang')
    input_text = session.get('text_clf_input') if session.get('text_clf_input') else ""
    labels = ['🥺 Sadness','😃 Joy', '😍 Love', '😡 Anger','😱 Fear','😯 Surprise']
    labels_short = ['Sad', 'Joy', 'Love', 'Anger', 'Fear', 'Surprise']
    sintam_labels = ['disgust','anger','fear','surprise','happy','sadness' ]
    sintam_labels_short = ['Disgust','Anger','Fear','Surprise','Joy','Sad' ]
    sentence1, sentence2 = session.get('s1'), session.get('s2')
    s1label, s2label = session.get('s1label'), session.get('s2label')
    predictions = session.get('text_predictions')
    augmented_sentences = session.get('augmented_sentences') if session.get('augmented_sentences') else []
    augmented_labels = session.get('augmented_labels') if session.get('augmented_labels') else []

    if request.method == 'POST':
        # text-classification logic
        if 'input_text' in request.form:
            input_text = request.form['input_text']
            model = request.form['model_select'].split('_')[0]
            lang = request.form['model_select'].split('_')[1]
            session['text_clf_input'] = input_text
            session['text_clf_model'] = model
            session['lang'] = lang
            pred, confs= make_prediction(input_text, model=model, lang=lang)
            c = confs.tolist()[0]
            if lang == 'en':
                predictions = {
                    l:round(c[i]*100, 2) for i, l in enumerate(labels)
                }
                prediction = labels[pred]
                session['text_clf_emotion'] = labels_short[pred]
            else:
                predictions = {
                    l:round(c[i]*100, 2) for i, l in enumerate(sintam_labels)
                }
                prediction=sintam_labels[pred]
                session['text_clf_emotion'] = sintam_labels_short[pred]
                
            session['text_predictions'] = predictions
            print('\nFunction: TEXT-CLF',
                  f'Model: {model}',
                  f'Input: {input_text}', 
                  f'Prediction: {prediction} @ {c[pred]*100:.2f}%\n', sep='\n')
            
        # data-augmentation logic
        elif 'sentence1' in request.form and 'sentence2' in request.form:
            print("\nFunction: DATA-AUG")
            sentence1 = request.form['sentence1']
            sentence2 = request.form['sentence2']
            s1label, s2label = request.form['sentence1_label'], request.form['sentence2_label']
            session['s1'] = sentence1
            session['s2'] = sentence2
            session['s1label'], session['s2label'] = s1label, s2label
            lang = request.form['shap_model']
            modname, tokname = "mod_en_89",'tok_en_89'
            # extract saliency
            s= time()
            t, p = int(request.form.get('thresh')), 0.6
            if lang=='en':
                train = pd.DataFrame({'text':[sentence1, sentence2], 'label':[labels.index(s1label) ,labels.index(s2label)]})
            else:
                labels_mapping = {'🥺 Sadness':'sadness','😃 Joy':'happy', '😍 Love':'disgust', '😡 Anger':'anger','😱 Fear':'fear','😯 Surprise':'surprise'}
                other_labels1, other_labels2 = labels_mapping[s1label], labels_mapping[s2label]
                train = pd.DataFrame({'text':[sentence1, sentence2], 'label':[sintam_labels.index(other_labels1) ,sintam_labels.index(other_labels2)]})
                
            if lang == 'en':
                shaps = get_shap_values_as_list(train, 'mod_en_6000', 'tok_en_6000')
            elif lang == 'si':
                shaps = get_shap_values_as_list(train, 'mod_og_si', 'tok_og_si')
            elif lang == 'tm':
                shaps = get_shap_values_as_list(train, 'mod_og_tm', 'tok_og_tm')
            shaps = [s.values for s in shaps]
            imp1 = [w for i, w in enumerate(sentence1.strip().split()) if shaps[0][i] >= np.percentile(shaps[0], t)]
            imp2 = [w for i, w in enumerate(sentence2.strip().split()) if shaps[1][i] >= np.percentile(shaps[1], t)]
            print(f"[1/3] ({time()-s:.2f}s)")
            print(f"Model: {model} -> {modname}, {tokname} | Threshold: {t}" )
            if lang=='en':
                print(f"Input 1: {str(sentence1)} / {s1label} -> {imp1}")
                print(f"Input 2: {str(sentence2)} / {s2label} -> {imp2}")
            else:
                print(f"Input 1: {str(sentence1)} / {other_labels1} -> {imp1}")
                print(f"Input 2: {str(sentence2)} / {other_labels2} -> {imp2}")
                
                
            s=time()
            # augment with SSwap
            factor=10
            augmented_sents = []
            for _ in range(0,factor):
                if bool(s1label == s2label):
                    aug_s = " ".join(SSwap().bisent_swap(sentence1, sentence2, shaps[0], shaps[1], t, True, p))
                else:
                    aug_s = " ".join(SSwap().bisent_swap(sentence1, sentence2, shaps[0], shaps[1], t, False, p))
                augmented_sents.append(aug_s)
            
            augmented_sentences = []
            augmented_labels = []
            print(f"[2/3] ({time()-s:.2f}s)")
            s=time()
            # classify augmented sentences
            for aug_s in augmented_sents:
                pred, confs= make_prediction(aug_s, lang=lang)
                augmented_sentences.append(aug_s)
                augmented_labels.append(labels[pred])
            session['augmented_sentences'] = augmented_sentences
            session['augmented_labels'] = augmented_labels
            print(f"Augmentations: {len(augmented_sentences)} contains {pd.Series(augmented_labels).unique()}")
            print(f"[3/3] ({time()-s:.2f}s)\n")
                    
    return render_template('text_classification.html', 
                           model_input=f"{model}_{lang}",
                           labels=labels, 
                           predictions=predictions, 
                           augmented_sentences=zip(augmented_sentences, augmented_labels),
                           input_text=input_text, sentence1=[sentence1, s1label], sentence2=[sentence2, s2label])


#########################################################################
# AUDIO-CLASSIFICATION
#########################################################################

@app.route('/audio_classification', methods=['POST', 'GET'])
@login_required
def audio_classification():
    if request.method == 'POST':
        classification_result = ""
        file = request.files['audio_file']

        return render_template('audio_classification.html', classification_result=classification_result)
    return render_template("audio_classification.html")


#########################################################################
# VIDEO-CLASSIFICATION
#########################################################################
# Emotion dictionary
emotion_dict = {0: "Angry", 1: "Disgusted", 2: "Fearful", 3: "Happy", 4: "Neutral", 5: "Sad", 6: "Surprised"}

# Load the model
json_file = open('image_model/emotion_model.json', 'r')
loaded_model_json = json_file.read()
json_file.close()
emotion_model = model_from_json(loaded_model_json)
emotion_model.load_weights("image_model/emotion_model.weights.h5")
print("Loaded model from disk")

# Initialize MTCNN detector and SHAP explainer
detector = MTCNN()
background = np.random.rand(1, 48, 48, 3)
explainer = shap.DeepExplainer(emotion_model, background)

# Variables for webcam recording
cap = None
generalized_emotion = None
total_frames = 0
emotion_counts = {}
confidence_sums = {}

@app.route('/image_classification')
@login_required
def image_classification():
    global generalized_emotion
    emotion = generalized_emotion  # Save the current emotion to a local variable
    generalized_emotion = None  # Reset after rendering
    return render_template('image_classification.html', emotion=emotion)


# Video streaming generator for webcam
def gen_frames():
    global cap, total_frames, emotion_counts, confidence_sums
    cap = cv2.VideoCapture(0)
    
    # Initialize emotion tracking
    emotion_counts = {emotion: 0 for emotion in emotion_dict.values()}
    confidence_sums = {emotion: 0 for emotion in emotion_dict.values()}
    prev_emotion = None

    while cap.isOpened():
        success, frame = cap.read()
        if not success:
            break
        else:
            # Process the frame
            frame, prev_emotion = process_frame(frame, emotion_model, explainer, emotion_dict, total_frames, [], emotion_counts, confidence_sums, prev_emotion)
            total_frames += 1

            ret, buffer = cv2.imencode('.jpg', frame)
            frame = buffer.tobytes()
            yield (b'--frame\r\n'
                   b'Content-Type: image/jpeg\r\n\r\n' + frame + b'\r\n')

    cap.release()

@app.route('/video_feed')
def video_feed():
    return Response(gen_frames(), mimetype='multipart/x-mixed-replace; boundary=frame')

@app.route('/stop', methods=['POST'])
def stop_webcam():
    global cap, generalized_emotion, emotion_counts, confidence_sums
    cap.release()
    generalized_emotion = calculate_generalized_emotion(emotion_counts, confidence_sums)
    session['image_clf_emotion']= generalized_emotion
    return redirect(url_for('image_classification'))

@app.route('/upload_video', methods=['POST'])
def upload_video():
    global generalized_emotion
    if 'file' not in request.files:
        return redirect(request.url)
    file = request.files['file']
    if file.filename == '':
        return redirect(request.url)
    if file:
        filepath = os.path.join(app.config['UPLOAD_FOLDER'], file.filename)
        file.save(filepath)
        # Process the video for emotion detection
        generalized_emotion = process_uploaded_video(filepath)
        session['image_clf_emotion']= generalized_emotion
        return redirect(url_for('image_classification'))

def process_uploaded_video(filepath):
    global emotion_counts, confidence_sums
    cap = cv2.VideoCapture(filepath)
    emotion_counts = {emotion: 0 for emotion in emotion_dict.values()}
    confidence_sums = {emotion: 0 for emotion in emotion_dict.values()}
    prev_emotion = None
    total_frames = 0

    frame_skip_rate = 25  # Process every 10th frame
    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break
        if total_frames % frame_skip_rate == 0:  # Process every nth frame
            frame, prev_emotion = process_frame(frame, emotion_model, explainer, emotion_dict, total_frames, [], emotion_counts, confidence_sums, prev_emotion)
        total_frames += 1

    cap.release()
    generalized_emotion = calculate_generalized_emotion(emotion_counts, confidence_sums)
    return generalized_emotion

######################################################################
# Recommendation-System

from flask import Flask
import keras
from utils import predict_recommendations , select_recomendation ,generate_random_data , generate_query_from_json_excluding
import numpy as np
from flask import request
import os
from huggingface_hub import InferenceClient
from openai import OpenAI


#API_KEY = os.getenv('OPEN_API_KEY')
client = OpenAI(
    api_key = 'sk-proj-vs8ZYEKwjk21LZVEYMN5T3BlbkFJBDntlT0yyJTMPS9XWi8h'
)


def chat_with_gpt(user_input= "hi Good Evening"):
    response = client.chat.completions.create(
        model="gpt-3.5-turbo",
        messages=[{"role": "user", "content": user_input}]
    )
    content = response.choices[0].message.content
    # print(response)
    return str(content)


os.environ['TF_ENABLE_ONEDNN_OPTS'] = '0'
print('Recommendation System: Libraries Imported')


model = keras.models.load_model('./models/dqn_model.keras')
# print('LOADED', model.summary())

@app.route('/recommend/check' , methods=['POST'])
@login_required
def load_and_pred():
    data = request.get_json()
    recomendation = data.get('rec') 
    random_data = generate_random_data()
    input_query = generate_query_from_json_excluding(random_data, recomendation)
    response = chat_with_gpt(input_query)
    return response

@app.route('/recommend', methods=['GET', 'POST'])
@login_required
def get_recomendation():
    if request.method == 'POST':
        # data = request.get_json()
        metadata = ["1 0"]  
        ratings = np.array([[[0]]]) 
        recommended_action = predict_recommendations(model, metadata, ratings)
        recomendation = select_recomendation(recommended_action)
        random_data = generate_random_data()
        random_data['negative state of emotion'] = request.form['emotion']
        input_query = generate_query_from_json_excluding(random_data, recomendation)
        response = chat_with_gpt(input_query)
        
        return_json = {
            "recomendation": recomendation,
            "description" : response
        }
    else:
        return_json = {
            "recomendation": None,
            "description": None,
        }
        
    emot = get_max_voted_emotion([session.get('text_clf_emotion'),
                                                        session.get('image_clf_emotion'),
                                                        session.get('audio_clf_emotion')])
    
    return render_template("recsys.html", 
                           rec=return_json, 
                           input=emot)


import MySQLdb

# Route for signup form
@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        name = request.form['name']
        email = request.form['email']
        password = request.form['password']
        hashed_password = generate_password_hash(password)  # Hash the password for security
        age = int(request.form['age'])
        sex = request.form['sex']
        location = request.form['location']
        # relationship_status = request.form['relationship_status']
        # designation = request.form['designation']
        # salary = int(request.form['salary'])
        # likes = request.form['likes']
        # dislikes = request.form['dislikes']
        # strengths = request.form['strengths']
        # weaknesses = request.form['weaknesses']
        # reason = request.form['reason']
        # suggestion = request.form['suggestion']

        new_user = User(
            name=name,
            email=email,
            password=hashed_password,  # Save hashed password
            age=age,
            sex=sex,
            location=location,
            # relationship_status=relationship_status,
            # designation=designation,
            # salary=salary,
            # likes=likes,
            # dislikes=dislikes,
            # strengths=strengths,
            # weaknesses=weaknesses,
            # reason=reason,
            # suggestion=suggestion
        )
        try:
            db.session.add(new_user)
            db.session.commit()
            print("[SIGNUP] New user created:", name, email)
            login_user(new_user)
            return render_template('index.html')
        except MySQLdb.IntegrityError:
            pass

    return render_template('register.html')

@app.route('/signin', methods=['GET', 'POST'])
def signin():
    if request.method == 'POST':
        email = request.form['email']
        password = request.form['password']

        user = User.query.filter_by(email=email).first()
        if user and check_password_hash(user.password, password):
            # Successful sign-in
            login_user(user)
            return render_template('index.html')
        else:
            print('Invalid email or password', 'error')

    return render_template('signin.html')
#####################################################
# Main 
if __name__ == '__main__':
    app.run(debug=False)
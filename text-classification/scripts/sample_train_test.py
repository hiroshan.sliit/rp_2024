import pandas as pd

def get_sample_df(df, n_rows=None, random_state=0):
    min_rows = df.label.value_counts().min()
    n_rows =  min_rows if n_rows == None else n_rows
    def sampleRows(group, x):
        if group.shape[0] > x:
            return group.sample(x, random_state=random_state)
        else:
            return group
    
    sample_df = df.groupby('label')[['label','text']].apply(
        lambda group: sampleRows(group, n_rows)
    ).droplevel(0)

    n_rows_req = n_rows * df.label.nunique()
    n_rows_sampled = sample_df.shape[0]
    n_rows_need = n_rows_req - n_rows_sampled

    df_rest = df.drop(list(sample_df.index), axis=0)
    sample_df_rest = df_rest.sample(n_rows_need, random_state=random_state)

    # Combine results
    sample_df_final = pd.concat([sample_df, sample_df_rest], axis=0)
    # print(sample_df_final.value_counts('label'))
    return sample_df_final

def create_train_partitions(df):
    print("Creating test sample ...")
    test_df = get_sample_df(df, 1000, 8)
    test_filename = 'test.csv' 
    test_df.to_csv('data/' + test_filename, index=False)
    print(f"🧪 Test Size: {test_df.shape}\n")

    df = df.drop(test_df.index).reset_index(drop=True) # Train set
    df = filter_df(df)
    df.to_csv('data/train.csv')

    print("Creating partitions ... ")
    n_labels = df.label.nunique()
    n_rows = df.shape[0]
    print(f"🎂 Original Size: {df.shape}\n")
    train_files = []
    for i in range(10, 101, 10):
        partition_rows = int((i* n_rows)/100)
        partition_sample = get_sample_df(df, int(partition_rows/n_labels), 8)
        print(f"🍰 Partition {i:3d}% : {partition_sample.shape[0]:5d} rows")
        filename = f"part_{i}.csv"
        partition_sample.to_csv('data/'+filename) # Save sample (subset of train.csv)
        train_files.append(filename)
    
    return train_files
        

def split_df(df, random_state=0):
    train_df = df.sample(frac=0.7, replace=False, random_state=random_state)
    sampled_indices = list(train_df.index)
    test_df = df.drop(sampled_indices, axis=0)
    return train_df, test_df

def filter_df(df):
    def custom_filter(text):
        tokens=text.strip().split(' ')
        return len(tokens) >= 2
    f_df = df[df.text.apply(custom_filter)]
    return f_df

def get_train_test(df, train_rows_per_label=200, test_rows_per_label=1000, n_labels=6, random_state=42):
    f_df = filter_df(df)
    train, test = split_df(f_df)
    train_sample = get_sample_df(train, n_rows=train_rows_per_label, random_state=random_state)
    test_sample = get_sample_df(test, n_rows=test_rows_per_label, random_state=random_state)
    return train_sample, test_sample





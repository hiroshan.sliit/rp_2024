transformers==4.39.3
torch==2.1.2
accelerate==0.29.3
gensim==4.3.2
nltk==3.2.4
clean-text[gpl]
tqdm
shap
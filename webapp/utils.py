import numpy as np
import tensorflow as tf

import random

def predict_recommendations(model, metadata_input, ratings_input):

    try:
     
        metadata_tensor = tf.convert_to_tensor(metadata_input, dtype=tf.string)
        metadata_tensor = tf.reshape(metadata_tensor, (1, 1))  

        ratings_tensor = tf.convert_to_tensor(ratings_input, dtype=tf.float32)  

        # Make predictions using the model
        q_values = model.predict([metadata_tensor, ratings_tensor])
        #print("Q-values:", q_values)

        # Get recommended actions (top 10 actions based on Q-values)
        #recommended_actions = np.argpartition(q_values[0], -10)[-10:]  # Get top 10 actions
        recommended_action = np.argmax(q_values[0])
        #print("Recommended Actions:", recommended_actions)

        return recommended_action
    
    

    except Exception as e:
        print("Error during prediction:", e)
        return None, None
    

def select_recomendation(index):
    recommendations = [
    "Engage in a hobby you love.",
    "Practice deep breathing exercises.",
    "Write down your feelings.",
    "Take short breaks regularly.",
    "Call a friend or family member."
    ]
    
    return recommendations[index]



def generate_random_data():
    data = {
        'age': random.randint(20, 60),
        'sex': random.choice(['M', 'F']),
        'location': random.choice(['Colombo', 'Kandy', 'Galle', 'Jaffna']),
        'Relationship Status': random.choice(['Single', 'In a relationship', 'Married', 'Divorced']),
        'designation': random.choice(['Software Engineer', 'Data Scientist', 'Web Developer', 'UI/UX Designer']),
        'salary': random.randint(50000, 100000),
        'likes': random.choice(['Reading', 'Traveling', 'Cooking', 'Gaming']),
        'dislikes': random.choice(['Traffic', 'Rainy days', 'Loud noises', 'Waiting in line']),
        'strengths': random.choice(['Problem-solving', 'Teamwork', 'Creativity', 'Adaptability']),
        'weaknesses': random.choice(['Procrastination', 'Perfectionism', 'Public speaking', 'Time management']),
        # 'negative state of emotion': random.choice(['Frustration', 'Anger', 'Anxiety', 'Sadness']),
        'reason': random.choice(['Long work hours', 'Workplace conflicts', 'Lack of recognition', 'Monotony']),
        'suggestion': random.choice(['Take a walk in nature', 'Practice mindfulness', 'Talk to a friend', 'Listen to music'])
    }
    return data


def generate_query_from_json_excluding(json_data, exclude_key):
    # Extract the values we need for the summary, excluding the specified key
    filtered_data = {k: v for k, v in json_data.items() if k != exclude_key}
    
    # Create the query with the selected data
    query = (
        f"Generate a short recomendation about {exclude_key} for a person with the below attributes: "
        f"Age: {filtered_data['age']}, Sex: {filtered_data['sex']}, Location: {filtered_data['location']}, "
        f"Relationship Status: {filtered_data['Relationship Status']}, what advice would you suggest?"
    )
    
    return query

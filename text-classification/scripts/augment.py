import nltk
nltk.download('stopwords')
import random
import numpy as np
import pandas as pd
from scripts.preprocess import tokenize_sent
from tqdm.notebook import tqdm_notebook as tqdm

class SSDA():
    def __init__(self, model, lang='en', bswap=False):
        if lang == 'en':
            self.wv_from_text = model
            self.stop_words = nltk.corpus.stopwords.words('english')
        elif lang == 'si':
            self.stop_words = []
            self.wv_from_text = model
            
        self.aug_methods = [
            self.random_swap,
            self.random_insertion,
            self.random_deletion,
            self.synonym_replacement,
        ]
        if bswap:
            self.aug_methods.append(self.bisent_swap) 
        
    # UTILS ############################################################

    def safe_divide(self, number, n):
        base_value, remainder = number // n, number % n
        result = [base_value] * n
        for i in range(remainder):
            result[i] += 1
        return result

    # COMMONS ###########################################################

    def get_synonyms(self, word):
        synonyms = set()
        flag = False
        vec = None
        try:
            vec = self.wv_from_text.similar_by_word(word.lower())
        except KeyError:
            flag = True
            pass
        if flag is False:
            synonyms.add(vec[0][0])
        if word in synonyms:
            synonyms.remove(word)
        return synonyms

    # RANDOM SWAP #######################################################

    def swap_word(self, new_words):
        random_idx_1 = random.randint(0, len(new_words) - 1)
        random_idx_2 = random_idx_1
        counter = 0
        while random_idx_2 == random_idx_1:
            random_idx_2 = random.randint(0, len(new_words) - 1)
            counter += 1
            if counter > 3:
                return new_words
        new_words[random_idx_1], new_words[random_idx_2] = new_words[random_idx_2], new_words[random_idx_1]
        return new_words

    def random_swap(self, words):
        new_words = words.copy()
        new_words = self.swap_word(new_words)
        return new_words

    # RANDOM INSERTION ###################################################

    def add_word(self, new_words):
        synonyms = []
        counter = 0

        while len(synonyms) <1:
            random_word = new_words[random.randint(0, len(new_words)-1)]
            #synonyms = self.synonyms_cadidates(random_word, self.df)
            synonyms = list(self.get_synonyms(random_word))
            counter += 1
            if counter > 10:
                return

        random_synonym = synonyms[0]
        random_idx = random.randint(0, len(new_words)-1)
        new_words.insert(random_idx, random_synonym)   

    def random_insertion(self, words):
        new_words = words.copy()
        self.add_word(new_words)
        return new_words

    # RANDOM DELETION ####################################################

    def random_deletion(self, words, words_to_ignore, p):
        thresh = p
        if len(words) == 1:
            return words
        new_words = []
        for word in words:
            if word in words_to_ignore:
                new_words.append(word)
            else:
                if random.uniform(0, 1) > thresh:
                    new_words.append(word)
                    
        if len(new_words) == 0:
            rand_int = random.randint(0, len(words)-1)
            return [words[rand_int]]
        
        return new_words

    # SYNONYM REPLACEMENT ################################################

    def synonym_replacement(self, words):
        new_words = words.copy()
        filtered_words = new_words
        random.shuffle(filtered_words)
        n_replaced = 0
        for w in filtered_words:
            synonyms = list(self.get_synonyms(w))
            if len(synonyms) >= 1:
                synonym = random.choice(synonyms)
                new_words = [synonym if word.lower() == w else word for word in new_words]
                n_replaced += 1
            if n_replaced >= 1:  # only replace up to n words
                break
        sentence = ' '.join(new_words)
        new_words = sentence.split(' ')
        return new_words

    # BI-SENTENCE WORD SWAP (⚠️ NEW METHOD) ################################

    def swap_words(self, sent1, sent2, shap_1, shap_2, thresh):
        words1 = tokenize_sent(sent1)
        words2 = tokenize_sent(sent2)
        # print(sent1, sent2, shap_1, shap_2, sep='\n')
        important_words_1 = [w for i, w in enumerate(words1) if shap_1[i] >= np.percentile(shap_1, thresh)]
        important_words_2 = [w for i, w in enumerate(words2) if shap_2[i] >= np.percentile(shap_2, thresh)]
        swappable_idx_1 = [idx for idx, w in enumerate(words1) if w not in important_words_1]
        swappable_idx_2 = [idx for idx, w in enumerate(words2) if w not in important_words_2]
        n = int(0.1 * max(1, min(len(swappable_idx_1), len(swappable_idx_2))))
        for _ in range(n):
            swap_idx_1, swap_idx_2 = random.choice(swappable_idx_1), random.choice(swappable_idx_2)
            words1[swap_idx_1], words2[swap_idx_2] = words2[swap_idx_2], words1[swap_idx_1]
        return (' '.join(words1), ' '.join(words2))

    def swap_imp_words(self, sent1, sent2, shap_1, shap_2 ,thresh):
        words1 = tokenize_sent(sent1)
        words2 = tokenize_sent(sent2)
        c=0
        stop=5
        # print(sent1, sent2, shap_1, shap_2, sep='\n')
        important_words_1 = [w for i, w in enumerate(words1) if shap_1[i] >= np.percentile(shap_1, thresh)]
        important_words_2 = [w for i, w in enumerate(words2) if shap_2[i] >= np.percentile(shap_2, thresh)]
        swappable_idx_1 = [idx for idx, w in enumerate(words1) if w in important_words_1]
        swappable_idx_2 = [idx for idx, w in enumerate(words2) if w in important_words_2]
        n = int(0.1 * max(1, min(len(swappable_idx_1), len(swappable_idx_2))))
        for _ in range(n):
            while sent1 == " ".join(words1):
                if c >= stop:
                    return None, None
                swap_idx_1, swap_idx_2 = random.choice(swappable_idx_1), random.choice(swappable_idx_2)
                words1[swap_idx_1], words2[swap_idx_2] = words2[swap_idx_2], words1[swap_idx_1]
                c+=1
        return (' '.join(words1), ' '.join(words2))


    def bisent_swap_words(self, df, shap_values, n_augs):
        aug_sents = []
        aug_labels = []
        for _ in tqdm(range(n_augs), desc="Bi-sentence Swap Progress"):
            for i in range(df.shape[0]):
                rand_idx = random.choice(df.drop(i, axis=0).index)
                sent1_aug, sent2_aug = self.swap_words(
                    df.text[i],
                    df.text[rand_idx],
                    shap_values[i],
                    shap_values[rand_idx],
                )
                aug_sents.extend([sent1_aug, sent2_aug])
                aug_labels.extend([df.label[i], df.label[rand_idx]])
        aug_df = pd.DataFrame({'text':aug_sents, 'label':aug_labels})
        return aug_df
    
    def bisent_swap(self, sent, rand_sent, shap, rand_shap, thresh, same_label=None):
        if not same_label:
            sent1, sent2 = self.swap_words(
                sent,
                rand_sent,
                shap,
                rand_shap, thresh
            )
        else:
            sent1, sent2 = self.swap_imp_words(
                sent,
                rand_sent,
                shap,
                rand_shap, thresh
            )
        return tokenize_sent(sent1)
        

    # NEW TECHNIQUE (SSDA)
    def stochastic_sequence_augmentation(self, sent, word_importance, factor, p, all_sents, all_shaps, meth_log, 
    labels, sent_label):
        augmented_sents = []
        
        words = tokenize_sent(sent)
        thresh = np.percentile(word_importance, 80)
        s_words = [w for i, w in enumerate(words) if word_importance[i] >= thresh]
        n = max(1, int(p * len(words)))
        
        bswap_selectable_idx = [i for i, s in enumerate(all_sents) if s != sent]
        
        for _ in range(0, factor):
            aug_seq = [random.choice(self.aug_methods) for _ in range(n)]
            new_words = words.copy()
            for aug_meth in aug_seq:
                aug_meth_name = aug_meth.__name__
                meth_log[aug_meth_name] += 1
                if aug_meth_name == "random_deletion":
                    new_words = aug_meth(words, s_words, p=p)
                elif aug_meth_name == 'bisent_swap':
                    rand_idx = random.choice(bswap_selectable_idx)
                    label  = labels[rand_idx]
                    rand_sent = all_sents[rand_idx]
                    rand_shap = all_shaps[rand_idx]
                    if label == sent_label:
                        new_words = aug_meth(" ".join(words), rand_sent, word_importance, rand_shap, same_label=True)
                    else:
                        new_words = aug_meth(" ".join(words), rand_sent, word_importance, rand_shap, same_label=False)
                else:
                    new_words = aug_meth(new_words)
            augmented_sents.append(" ".join(new_words))
        return augmented_sents

    def just_bswap(self, sent, word_importance, factor, p, all_sents, all_shaps,labels, sent_label, t):
        augmented_sents = []
        
        words = tokenize_sent(sent)
        thresh = np.percentile(word_importance, 80)
        s_words = [w for i, w in enumerate(words) if word_importance[i] >= thresh]
        n = max(1, int(p * len(words)))
        
        bswap_selectable_idx = [i for i, s in enumerate(all_sents) if s != sent]
        
        for _ in range(0, factor):
            new_words = words.copy()
            rand_idx = random.choice(bswap_selectable_idx)
            label  = labels[rand_idx]
            rand_sent = all_sents[rand_idx]
            rand_shap = all_shaps[rand_idx]
            if label == sent_label:
                new_words = self.bisent_swap(" ".join(words), rand_sent, word_importance, rand_shap, t, same_label=True)
            else:
                new_words = self.bisent_swap(" ".join(words), rand_sent, word_importance, rand_shap, t, same_label=False)
            augmented_sents.append(" ".join(new_words))
        return augmented_sents
                    
    def augment_data(self, df, shap_values, factor, p=0.1):
        meth_log = {meth.__name__:0 for meth in self.aug_methods}
        print(f"\n{'-'*20} Augentation Process Started {'-'*20}\n")
        print(f"\nOriginal: {df.shape[0]} rows")
        
        sents = df.text.to_list()
        labels = df.label.to_list()
        shap_values_per_sent = shap_values
        aug_df = pd.DataFrame(columns=['text', 'label'])
        
        for i, sent in tqdm(enumerate(sents), desc="DDA Progress", total=len(sents)):
            word_importance = shap_values_per_sent[i]
            augmented_sents = self.stochastic_sequence_augmentation(
                sent, word_importance, factor, p, sents, shap_values, meth_log, labels, labels[i]
            )
            augmented_sents_labels = [labels[i]] * len(augmented_sents) # same labels for all augs
            aug_df = pd.concat([aug_df, pd.DataFrame({
                'text':augmented_sents,
                'label':augmented_sents_labels
            })])
            
        # Add original df to augmented df
        full_aug_df = pd.concat([df, aug_df], axis=0).reset_index(drop=True)
        
        print(meth_log)
        print(f"Post-augmentation: {full_aug_df.shape[0]} rows")
        print(f"\n{'-'*20} Augentation Process Completed {'-'*20}\n")
        return full_aug_df       

    def augment_data_bswap(self, df, shap_values, factor, p=0.1, t=80):
        meth_log = {meth.__name__:0 for meth in self.aug_methods}
        print(f"\n{'-'*20} Augentation Process Started {'-'*20}\n")
        print(f"\nOriginal: {df.shape[0]} rows")
        
        sents = df.text.to_list()
        labels = df.label.to_list()
        shap_values_per_sent = shap_values
        aug_df = pd.DataFrame(columns=['text', 'label'])

        for i, sent in tqdm(enumerate(sents), desc="DDA Progress", total=len(sents)):
            word_importance = shap_values_per_sent[i]
            augmented_sents = self.just_bswap(
                sent, word_importance, factor, p, sents, shap_values, labels, labels[i], t
            )
            augmented_sents_labels = [labels[i]] * len(augmented_sents) # same labels for all augs
            aug_df = pd.concat([aug_df, pd.DataFrame({
                'text':augmented_sents,
                'label':augmented_sents_labels
            })])
            
        # Add original df to augmented df
        full_aug_df = pd.concat([df, aug_df], axis=0).reset_index(drop=True)
        
        print(meth_log)
        print(f"Post-augmentation: {full_aug_df.shape[0]} rows")
        print(f"\n{'-'*20} Augentation Process Completed {'-'*20}\n")
        return full_aug_df   

class SSwap():
    def swap_words(self, sent1, sent2, shap_1, shap_2, thresh):
        words1 = tokenize_sent(sent1)
        words2 = tokenize_sent(sent2)
        # print(sent1, sent2, shap_1, shap_2, sep='\n')
        important_words_1 = [w for i, w in enumerate(words1) if shap_1[i] >= np.percentile(shap_1, thresh)]
        important_words_2 = [w for i, w in enumerate(words2) if shap_2[i] >= np.percentile(shap_2, thresh)]
        swappable_idx_1 = [idx for idx, w in enumerate(words1) if w not in important_words_1]
        swappable_idx_2 = [idx for idx, w in enumerate(words2) if w not in important_words_2]
        n = int(0.1 * max(1, min(len(swappable_idx_1), len(swappable_idx_2))))
        for _ in range(n):
            swap_idx_1, swap_idx_2 = random.choice(swappable_idx_1), random.choice(swappable_idx_2)
            words1[swap_idx_1], words2[swap_idx_2] = words2[swap_idx_2], words1[swap_idx_1]
        return (' '.join(words1), ' '.join(words2))

    def swap_imp_words(self, sent1, sent2, shap_1, shap_2 ,thresh):
        words1 = tokenize_sent(sent1)
        words2 = tokenize_sent(sent2)
        c=0
        stop=5
        # print(sent1, sent2, shap_1, shap_2, sep='\n')
        important_words_1 = [w for i, w in enumerate(words1) if shap_1[i] >= np.percentile(shap_1, thresh)]
        important_words_2 = [w for i, w in enumerate(words2) if shap_2[i] >= np.percentile(shap_2, thresh)]
        swappable_idx_1 = [idx for idx, w in enumerate(words1) if w in important_words_1]
        swappable_idx_2 = [idx for idx, w in enumerate(words2) if w in important_words_2]
        n = int(0.1 * max(1, min(len(swappable_idx_1), len(swappable_idx_2))))
        for _ in range(n):
            while sent1 == " ".join(words1):
                if c >= stop:
                    return None, None
                swap_idx_1, swap_idx_2 = random.choice(swappable_idx_1), random.choice(swappable_idx_2)
                words1[swap_idx_1], words2[swap_idx_2] = words2[swap_idx_2], words1[swap_idx_1]
                c+=1
        return (' '.join(words1), ' '.join(words2))   

    def bisent_swap(self, sent, rand_sent, shap, rand_shap, thresh, same_label=None):
        if not same_label:
            sent1, sent2 = self.swap_words(
                sent,
                rand_sent,
                shap,
                rand_shap, thresh
            )
        else:
            sent1, sent2 = self.swap_imp_words(
                sent,
                rand_sent,
                shap,
                rand_shap, thresh
            )
        return tokenize_sent(sent1)

    def just_bswap(self, sent, word_importance, factor, p, all_sents, all_shaps,labels, sent_label, t):
        augmented_sents = [sent]
        
        words = tokenize_sent(sent)
        thresh = np.percentile(word_importance, 80)
        s_words = [w for i, w in enumerate(words) if word_importance[i] >= thresh]
        n = max(1, int(p * len(words)))
        
        bswap_selectable_idx = [i for i, s in enumerate(all_sents) if s != sent]
        
        for _ in range(0, factor):
            new_words = words.copy()
            rand_idx = random.choice(bswap_selectable_idx)
            label  = labels[rand_idx]
            rand_sent = all_sents[rand_idx]
            rand_shap = all_shaps[rand_idx]
            if label == sent_label:
                new_words = self.bisent_swap(" ".join(words), rand_sent, word_importance, rand_shap, t, same_label=True)
            else:
                new_words = self.bisent_swap(" ".join(words), rand_sent, word_importance, rand_shap, t, same_label=False)
            augmented_sents.append(" ".join(new_words))
        return augmented_sents    

    def augment_data_bswap(self, df, shap_values, factor, p=0.1, t=80):
        print(f"\nOriginal: {df.shape[0]} rows")
        
        sents = df.text.to_list()
        labels = df.label.to_list()
        shap_values_per_sent = shap_values
        aug_df = pd.DataFrame(columns=['text', 'label'])

        for i, sent in tqdm(enumerate(sents), desc="SSwap Progress", total=len(sents)):
            word_importance = shap_values_per_sent[i]
            augmented_sents = self.just_bswap(
                sent, word_importance, factor, p, sents, shap_values, labels, labels[i], t
            )
            augmented_sents_labels = [labels[i]] * len(augmented_sents) # same labels for all augs
            aug_df = pd.concat([aug_df, pd.DataFrame({
                'text':augmented_sents,
                'label':augmented_sents_labels
            })])
            
        # Add original df to augmented df
        # full_aug_df = pd.concat([df, aug_df], axis=0).reset_index(drop=True)
        
        print(f"Post-augmentation: {aug_df.shape[0]} rows")
        return aug_df  


import nltk
nltk.download('stopwords')
import random
import numpy as np
import pandas as pd
from scripts.preprocess import tokenize_sent
from tqdm.notebook import tqdm_notebook as tqdm

class SSDA():
    def __init__(self, model, lang='en', bswap=False):
        if lang == 'en':
            self.wv_from_text = model
            self.stop_words = nltk.corpus.stopwords.words('english')
        elif lang == 'si':
            self.stop_words = []
            self.wv_from_text = model
            
        self.aug_methods = [
            self.random_swap,
            self.random_insertion,
            self.random_deletion,
            self.synonym_replacement,
        ]
        if bswap:
            self.aug_methods.append(self.bisent_swap) 
        
    # UTILS ############################################################

    def safe_divide(self, number, n):
        base_value, remainder = number // n, number % n
        result = [base_value] * n
        for i in range(remainder):
            result[i] += 1
        return result

    # COMMONS ###########################################################

    def get_synonyms(self, word):
        synonyms = set()
        flag = False
        vec = None
        try:
            vec = self.wv_from_text.similar_by_word(word.lower())
        except KeyError:
            flag = True
            pass
        if flag is False:
            synonyms.add(vec[0][0])
        if word in synonyms:
            synonyms.remove(word)
        return synonyms

    # RANDOM SWAP #######################################################

    def swap_word(self, new_words):
        random_idx_1 = random.randint(0, len(new_words) - 1)
        random_idx_2 = random_idx_1
        counter = 0
        while random_idx_2 == random_idx_1:
            random_idx_2 = random.randint(0, len(new_words) - 1)
            counter += 1
            if counter > 3:
                return new_words
        new_words[random_idx_1], new_words[random_idx_2] = new_words[random_idx_2], new_words[random_idx_1]
        return new_words

    def random_swap(self, words):
        new_words = words.copy()
        new_words = self.swap_word(new_words)
        return new_words

    # RANDOM INSERTION ###################################################

    def add_word(self, new_words):
        synonyms = []
        counter = 0

        while len(synonyms) <1:
            random_word = new_words[random.randint(0, len(new_words)-1)]
            #synonyms = self.synonyms_cadidates(random_word, self.df)
            synonyms = list(self.get_synonyms(random_word))
            counter += 1
            if counter > 10:
                return

        random_synonym = synonyms[0]
        random_idx = random.randint(0, len(new_words)-1)
        new_words.insert(random_idx, random_synonym)   

    def random_insertion(self, words):
        new_words = words.copy()
        self.add_word(new_words)
        return new_words

    # RANDOM DELETION ####################################################

    def random_deletion(self, words, words_to_ignore, p):
        thresh = p
        if len(words) == 1:
            return words
        new_words = []
        for word in words:
            if word in words_to_ignore:
                new_words.append(word)
            else:
                if random.uniform(0, 1) > thresh:
                    new_words.append(word)
                    
        if len(new_words) == 0:
            rand_int = random.randint(0, len(words)-1)
            return [words[rand_int]]
        
        return new_words

    # SYNONYM REPLACEMENT ################################################

    def synonym_replacement(self, words):
        new_words = words.copy()
        filtered_words = new_words
        random.shuffle(filtered_words)
        n_replaced = 0
        for w in filtered_words:
            synonyms = list(self.get_synonyms(w))
            if len(synonyms) >= 1:
                synonym = random.choice(synonyms)
                new_words = [synonym if word.lower() == w else word for word in new_words]
                n_replaced += 1
            if n_replaced >= 1:  # only replace up to n words
                break
        sentence = ' '.join(new_words)
        new_words = sentence.split(' ')
        return new_words

    # BI-SENTENCE WORD SWAP (⚠️ NEW METHOD) ################################

    def swap_words(self, sent1, sent2, shap_1, shap_2, thresh):
        words1 = tokenize_sent(sent1)
        words2 = tokenize_sent(sent2)
        # print(sent1, sent2, shap_1, shap_2, sep='\n')
        important_words_1 = [w for i, w in enumerate(words1) if shap_1[i] >= np.percentile(shap_1, thresh)]
        important_words_2 = [w for i, w in enumerate(words2) if shap_2[i] >= np.percentile(shap_2, thresh)]
        swappable_idx_1 = [idx for idx, w in enumerate(words1) if w not in important_words_1]
        swappable_idx_2 = [idx for idx, w in enumerate(words2) if w not in important_words_2]
        n = int(0.1 * max(1, min(len(swappable_idx_1), len(swappable_idx_2))))
        for _ in range(n):
            swap_idx_1, swap_idx_2 = random.choice(swappable_idx_1), random.choice(swappable_idx_2)
            words1[swap_idx_1], words2[swap_idx_2] = words2[swap_idx_2], words1[swap_idx_1]
        return (' '.join(words1), ' '.join(words2))

    def swap_imp_words(self, sent1, sent2, shap_1, shap_2 ,thresh):
        words1 = tokenize_sent(sent1)
        words2 = tokenize_sent(sent2)
        c=0
        stop=5
        # print(sent1, sent2, shap_1, shap_2, sep='\n')
        important_words_1 = [w for i, w in enumerate(words1) if shap_1[i] >= np.percentile(shap_1, thresh)]
        important_words_2 = [w for i, w in enumerate(words2) if shap_2[i] >= np.percentile(shap_2, thresh)]
        swappable_idx_1 = [idx for idx, w in enumerate(words1) if w in important_words_1]
        swappable_idx_2 = [idx for idx, w in enumerate(words2) if w in important_words_2]
        n = int(0.1 * max(1, min(len(swappable_idx_1), len(swappable_idx_2))))
        for _ in range(n):
            while sent1 == " ".join(words1):
                if c >= stop:
                    return None, None
                swap_idx_1, swap_idx_2 = random.choice(swappable_idx_1), random.choice(swappable_idx_2)
                words1[swap_idx_1], words2[swap_idx_2] = words2[swap_idx_2], words1[swap_idx_1]
                c+=1
        return (' '.join(words1), ' '.join(words2))


    def bisent_swap_words(self, df, shap_values, n_augs):
        aug_sents = []
        aug_labels = []
        for _ in tqdm(range(n_augs), desc="Bi-sentence Swap Progress"):
            for i in range(df.shape[0]):
                rand_idx = random.choice(df.drop(i, axis=0).index)
                sent1_aug, sent2_aug = self.swap_words(
                    df.text[i],
                    df.text[rand_idx],
                    shap_values[i],
                    shap_values[rand_idx],
                )
                aug_sents.extend([sent1_aug, sent2_aug])
                aug_labels.extend([df.label[i], df.label[rand_idx]])
        aug_df = pd.DataFrame({'text':aug_sents, 'label':aug_labels})
        return aug_df
    
    def bisent_swap(self, sent, rand_sent, shap, rand_shap, thresh, same_label=None):
        if not same_label:
            sent1, sent2 = self.swap_words(
                sent,
                rand_sent,
                shap,
                rand_shap, thresh
            )
        else:
            sent1, sent2 = self.swap_imp_words(
                sent,
                rand_sent,
                shap,
                rand_shap, thresh
            )
        return tokenize_sent(sent1)
        

    # NEW TECHNIQUE (SSDA)
    def stochastic_sequence_augmentation(self, sent, word_importance, factor, p, all_sents, all_shaps, meth_log, 
    labels, sent_label):
        augmented_sents = []
        
        words = tokenize_sent(sent)
        thresh = np.percentile(word_importance, 80)
        s_words = [w for i, w in enumerate(words) if word_importance[i] >= thresh]
        n = max(1, int(p * len(words)))
        
        bswap_selectable_idx = [i for i, s in enumerate(all_sents) if s != sent]
        
        for _ in range(0, factor):
            aug_seq = [random.choice(self.aug_methods) for _ in range(n)]
            new_words = words.copy()
            for aug_meth in aug_seq:
                aug_meth_name = aug_meth.__name__
                meth_log[aug_meth_name] += 1
                if aug_meth_name == "random_deletion":
                    new_words = aug_meth(words, s_words, p=p)
                elif aug_meth_name == 'bisent_swap':
                    rand_idx = random.choice(bswap_selectable_idx)
                    label  = labels[rand_idx]
                    rand_sent = all_sents[rand_idx]
                    rand_shap = all_shaps[rand_idx]
                    if label == sent_label:
                        new_words = aug_meth(" ".join(words), rand_sent, word_importance, rand_shap, same_label=True)
                    else:
                        new_words = aug_meth(" ".join(words), rand_sent, word_importance, rand_shap, same_label=False)
                else:
                    new_words = aug_meth(new_words)
            augmented_sents.append(" ".join(new_words))
        return augmented_sents

    def just_bswap(self, sent, word_importance, factor, p, all_sents, all_shaps,labels, sent_label, t):
        augmented_sents = []
        
        words = tokenize_sent(sent)
        thresh = np.percentile(word_importance, 80)
        s_words = [w for i, w in enumerate(words) if word_importance[i] >= thresh]
        n = max(1, int(p * len(words)))
        
        bswap_selectable_idx = [i for i, s in enumerate(all_sents) if s != sent]
        
        for _ in range(0, factor):
            new_words = words.copy()
            rand_idx = random.choice(bswap_selectable_idx)
            label  = labels[rand_idx]
            rand_sent = all_sents[rand_idx]
            rand_shap = all_shaps[rand_idx]
            if label == sent_label:
                new_words = self.bisent_swap(" ".join(words), rand_sent, word_importance, rand_shap, t, same_label=True)
            else:
                new_words = self.bisent_swap(" ".join(words), rand_sent, word_importance, rand_shap, t, same_label=False)
            augmented_sents.append(" ".join(new_words))
        return augmented_sents
                    
    def augment_data(self, df, shap_values, factor, p=0.1):
        meth_log = {meth.__name__:0 for meth in self.aug_methods}
        print(f"\n{'-'*20} Augentation Process Started {'-'*20}\n")
        print(f"\nOriginal: {df.shape[0]} rows")
        
        sents = df.text.to_list()
        labels = df.label.to_list()
        shap_values_per_sent = shap_values
        aug_df = pd.DataFrame(columns=['text', 'label'])
        
        for i, sent in tqdm(enumerate(sents), desc="DDA Progress", total=len(sents)):
            word_importance = shap_values_per_sent[i]
            augmented_sents = self.stochastic_sequence_augmentation(
                sent, word_importance, factor, p, sents, shap_values, meth_log, labels, labels[i]
            )
            augmented_sents_labels = [labels[i]] * len(augmented_sents) # same labels for all augs
            aug_df = pd.concat([aug_df, pd.DataFrame({
                'text':augmented_sents,
                'label':augmented_sents_labels
            })])
            
        # Add original df to augmented df
        full_aug_df = pd.concat([df, aug_df], axis=0).reset_index(drop=True)
        
        print(meth_log)
        print(f"Post-augmentation: {full_aug_df.shape[0]} rows")
        print(f"\n{'-'*20} Augentation Process Completed {'-'*20}\n")
        return full_aug_df       

    def augment_data_bswap(self, df, shap_values, factor, p=0.1, t=80):
        meth_log = {meth.__name__:0 for meth in self.aug_methods}
        print(f"\n{'-'*20} Augentation Process Started {'-'*20}\n")
        print(f"\nOriginal: {df.shape[0]} rows")
        
        sents = df.text.to_list()
        labels = df.label.to_list()
        shap_values_per_sent = shap_values
        aug_df = pd.DataFrame(columns=['text', 'label'])

        for i, sent in tqdm(enumerate(sents), desc="DDA Progress", total=len(sents)):
            word_importance = shap_values_per_sent[i]
            augmented_sents = self.just_bswap(
                sent, word_importance, factor, p, sents, shap_values, labels, labels[i], t
            )
            augmented_sents_labels = [labels[i]] * len(augmented_sents) # same labels for all augs
            aug_df = pd.concat([aug_df, pd.DataFrame({
                'text':augmented_sents,
                'label':augmented_sents_labels
            })])
            
        # Add original df to augmented df
        full_aug_df = pd.concat([df, aug_df], axis=0).reset_index(drop=True)
        
        print(meth_log)
        print(f"Post-augmentation: {full_aug_df.shape[0]} rows")
        print(f"\n{'-'*20} Augentation Process Completed {'-'*20}\n")
        return full_aug_df   

class SSwap():
    def swap_words(self, sent1, sent2, shap_1, shap_2, thresh):
        words1 = tokenize_sent(sent1)
        words2 = tokenize_sent(sent2)
        # print(sent1, sent2, shap_1, shap_2, sep='\n')
        important_words_1 = [w for i, w in enumerate(words1) if shap_1[i] >= np.percentile(shap_1, thresh)]
        important_words_2 = [w for i, w in enumerate(words2) if shap_2[i] >= np.percentile(shap_2, thresh)]
        swappable_idx_1 = [idx for idx, w in enumerate(words1) if w not in important_words_1]
        swappable_idx_2 = [idx for idx, w in enumerate(words2) if w not in important_words_2]
        n = int(0.1 * max(1, min(len(swappable_idx_1), len(swappable_idx_2))))
        for _ in range(n):
            swap_idx_1, swap_idx_2 = random.choice(swappable_idx_1), random.choice(swappable_idx_2)
            words1[swap_idx_1], words2[swap_idx_2] = words2[swap_idx_2], words1[swap_idx_1]
        return (' '.join(words1), ' '.join(words2))

    def swap_imp_words(self, sent1, sent2, shap_1, shap_2 ,thresh):
        words1 = tokenize_sent(sent1)
        words2 = tokenize_sent(sent2)
        c=0
        stop=5
        # print(sent1, sent2, shap_1, shap_2, sep='\n')
        important_words_1 = [w for i, w in enumerate(words1) if shap_1[i] >= np.percentile(shap_1, thresh)]
        important_words_2 = [w for i, w in enumerate(words2) if shap_2[i] >= np.percentile(shap_2, thresh)]
        swappable_idx_1 = [idx for idx, w in enumerate(words1) if w in important_words_1]
        swappable_idx_2 = [idx for idx, w in enumerate(words2) if w in important_words_2]
        n = int(0.1 * max(1, min(len(swappable_idx_1), len(swappable_idx_2))))
        for _ in range(n):
            while sent1 == " ".join(words1):
                if c >= stop:
                    return None, None
                swap_idx_1, swap_idx_2 = random.choice(swappable_idx_1), random.choice(swappable_idx_2)
                words1[swap_idx_1], words2[swap_idx_2] = words2[swap_idx_2], words1[swap_idx_1]
                c+=1
        return (' '.join(words1), ' '.join(words2))   

    def bisent_swap(self, sent, rand_sent, shap, rand_shap, thresh, same_label=None):
        if not same_label:
            sent1, sent2 = self.swap_words(
                sent,
                rand_sent,
                shap,
                rand_shap, thresh
            )
        else:
            sent1, sent2 = self.swap_imp_words(
                sent,
                rand_sent,
                shap,
                rand_shap, thresh
            )
        return tokenize_sent(sent1)

    def just_bswap(self, sent, word_importance, factor, p, all_sents, all_shaps,labels, sent_label, t):
        augmented_sents = [sent]
        
        words = tokenize_sent(sent)
        thresh = np.percentile(word_importance, 80)
        s_words = [w for i, w in enumerate(words) if word_importance[i] >= thresh]
        n = max(1, int(p * len(words)))
        
        bswap_selectable_idx = [i for i, s in enumerate(all_sents) if s != sent]
        
        for _ in range(0, factor):
            new_words = words.copy()
            rand_idx = random.choice(bswap_selectable_idx)
            label  = labels[rand_idx]
            rand_sent = all_sents[rand_idx]
            rand_shap = all_shaps[rand_idx]
            if label == sent_label:
                new_words = self.bisent_swap(" ".join(words), rand_sent, word_importance, rand_shap, t, same_label=True)
            else:
                new_words = self.bisent_swap(" ".join(words), rand_sent, word_importance, rand_shap, t, same_label=False)
            augmented_sents.append(" ".join(new_words))
        return augmented_sents    

    def augment_data_bswap(self, df, shap_values, factor, p=0.1, t=80):
        print(f"\nOriginal: {df.shape[0]} rows")
        
        sents = df.text.to_list()
        labels = df.label.to_list()
        shap_values_per_sent = shap_values
        aug_df = pd.DataFrame(columns=['text', 'label'])

        for i, sent in tqdm(enumerate(sents), desc="SSwap Progress", total=len(sents)):
            word_importance = shap_values_per_sent[i]
            augmented_sents = self.just_bswap(
                sent, word_importance, factor, p, sents, shap_values, labels, labels[i], t
            )
            augmented_sents_labels = [labels[i]] * len(augmented_sents) # same labels for all augs
            aug_df = pd.concat([aug_df, pd.DataFrame({
                'text':augmented_sents,
                'label':augmented_sents_labels
            })])
            
        # Add original df to augmented df
        # full_aug_df = pd.concat([df, aug_df], axis=0).reset_index(drop=True)
        
        print(f"Post-augmentation: {aug_df.shape[0]} rows")
        return aug_df  



class VDDA():
    """
    @author: Mosleh Mahamud  
    @repo: https://github.com/mosh98/swe_aug 
    @changes:
        - Adapted to English language
        - Removed word-based synonym replacement
        - Add sentence rejections
        - Displaying augmentation counts
    """
    def __init__(self, model, language='en'):
        # initialize
        if language == 'en':
            self.stop_words_ = set(nltk.corpus.stopwords.words('english'))
            self.wv_from_text = model
        elif language == 'sin':
            self.stop_words_ = []
            self.wv_from_text = model


 
    def synonym_replacement_vec(self,words, n):
        new_words = words.copy()
        random_word_list = list(set([word for word in words if word not in self.stop_words_]))
        random.shuffle(random_word_list)
        num_replaced = 0
        for random_word in random_word_list:
            synonyms = self.get_synonyms_vec(random_word)
            if len(synonyms) >= 1:
                synonym = random.choice(list(synonyms))
                new_words = [synonym if word.lower() == random_word else word for word in new_words]
                # print("replaced", random_word, "with", synonym)
                num_replaced += 1
            if num_replaced >= n:  # only replace up to n words
                break

        # this is stupid but we need it, trust me
        sentence = ' '.join(new_words)
        new_words = sentence.split(' ')
        return new_words

    def get_synonyms_vec(self,word):
        synonyms = set()
        flag = False
        vec = None
        try:
            vec = self.wv_from_text.similar_by_word(word.lower())
        except KeyError:
            flag = True
            pass

        if flag is False:
            synonyms.add(vec[0][0])

        if word in synonyms:
            synonyms.remove(word)

        return synonyms

    #def random_insertion(self, words, p):
    def random_insertion(self, words, n):
        """
        Randomly insert words into a sentence with probability p
        :param words:
        :param p:
        :return:
        """
        new_words = words.copy()
        for _ in range(n):
                self.add_word(new_words)

        return new_words

    def add_word(self, new_words):
        synonyms = []
        counter = 0

        while len(synonyms) <1:
            random_word = new_words[random.randint(0, len(new_words)-1)]
            #synonyms = self.synonyms_cadidates(random_word, self.df)
            synonyms = list(self.get_synonyms_vec(random_word))
            counter += 1
            if counter > 10:
                return

        random_synonym = synonyms[0]
        random_idx = random.randint(0, len(new_words)-1)
        new_words.insert(random_idx, random_synonym)


    def enkel_augmentation(self, sentence, count, alpha_sr=0.1, alpha_ri=0.1, alpha_rs=0.1, alpha_rd=0.1, num_aug=9):

        """
        @param sentence
        @param alpha_sr synonym replacement rate, percentage of the total sentence
        @param alpha_ri random insertion rate, percentage of the total sentence
        @param alpha_rs random swap rate, percentage of the total sentence
        @param alpha_rd random deletion rate, percentage of the total sentence
        @param num_aug how many augmented sentences to create

        inspired from : https://github.com/jasonwei20/eda_nlp/blob/04ab29c5b18d2d72f9fa5b304322aaf4793acea0/code/eda.py#L33
        @return list of augmented sentences
        
        """
        words_list = sentence.split(' ')  # list of words in the sentence
        words = [word for word in words_list if word != '']  # remove empty words
        num_words = len(words_list)  # number of words in the sentence

        augmented_sentences = [sentence]
        num_new_per_technique = int(num_aug / 2) # number of augmented sentences per technique

        #synonmym replacement
        if (alpha_sr > 0):
            n_sr = max(1, int(alpha_sr * num_words)) # number of words to be replaced per technique
            #print("Number of words to be replaced per technique: ", n_sr)
            for _ in range(num_new_per_technique):
                a_words = self.synonym_replacement_vec(words, n_sr)
                augmented_sentences.append(' '.join(a_words))
                count['synonym_replacement'] += 1

        #random insertion
        if (alpha_ri > 0):
            n_ri = max(1,int(alpha_ri * num_words))
            for _ in range(num_new_per_technique):
                a_words = self.random_insertion(words, n_ri)
                augmented_sentences.append(' '.join(a_words))
                count['random_insertion'] += 1

        return augmented_sentences
    
    def augment_df(self, df, n_aug):
        aug_df = pd.DataFrame(columns=['text', 'label'])
        n_sents = df.shape[0]
        count = {
            'random_insertion':0,
            'synonym_replacement':0,
        }
        for _, row in tqdm(df.iterrows(), total=df.shape[0]):
            label = row['label']
            aug_sents = self.enkel_augmentation(
                row['text'], 
                alpha_sr=0.2, alpha_ri=0.2, 
                alpha_rs=0.2, alpha_rd=0.2, 
                num_aug=n_aug, count=count
            )
            aug_df = pd.concat([
                aug_df, pd.DataFrame({
                    'text':aug_sents,
                    'label':[label]*len(aug_sents)})
            ], axis=0)
        n_aug_sents = aug_df.shape[0]
        print(f"\nOriginal: {df.shape[0]} rows")
        print(f"Augmented [Vanilla DDA]: {n_aug_sents} rows")
        print(
            f"- Random Insertion: {count['random_insertion']}",
            f"- Synonym Replacement: {count['synonym_replacement']}", 
            sep='\n')
        return aug_df

 

# import os
# import numpy as np
# import librosa
# from keras.models import load_model
# from pydub import AudioSegment
# from scipy.io import wavfile
# from pydub.effects import normalize
# import noisereduce as nr

# # Function to preprocess audio file
# def preprocess_audio_file(path):
#     # Step 1: Load the audio file into an 'AudioSegment' object, and extract the sample rate.
#     rawsound = AudioSegment.from_file(path)
#     sr, x = wavfile.read(path)

#     # Step 2: Normalize to +5.0 dBFS, Transform audio signals to an array.
#     normalizedsound = normalize(rawsound, headroom=5.0)
#     normal_x = np.array(normalizedsound.get_array_of_samples(), dtype='float32')

#     # Step 3: Trim silence in the beginning and end.
#     xt, index = librosa.effects.trim(normal_x, top_db=30)
#     print("Length after trimming:", len(xt))  # Debugging: Print length after trimming

#     if len(xt) == 0:
#         print("Warning: Trimmed audio length is zero.")
#         return None, sr

#     # Step 4: Right-side padding for length equalization.
#     max_length = 173056  # maximum lengthed audio
#     padded_x = np.pad(xt, (0, max_length - len(xt)), 'constant')

#     # Step 5: Reduce noise
#     final_x = nr.reduce_noise(y=padded_x, sr=sr)

#     return final_x, sr

# # # Iterate through each file in the dataset
# # for subdir, dirs, files in os.walk(ravdess_data_path):
# #     for file in files:
# #         # Check if file is a .wav file
# #         if file.endswith('.wav'):
# #             file_path = os.path.join(subdir, file)
# #             print("Processing:", file_path)

# #             # Preprocess the audio file
# #             processed_audio, sample_rate = preprocess_audio_file(file_path)

# #             if processed_audio is None:
# #                 print("Skipping file due to empty trimmed audio.")
# #                 continue

# #             # Display or save the processed audio if needed
# #             # For example, display the spectrogram
# #             plt.figure(figsize=(10, 4))
# #             librosa.display.specshow(librosa.amplitude_to_db(np.abs(librosa.stft(processed_audio)), ref=np.max), sr=sample_rate, x_axis='time', y_axis='log')
# #             plt.colorbar(format='%+2.0f dB')
# #             plt.title('Spectrogram')
# #             plt.show()

# #             # Play the processed audio
# #             ipd.display(ipd.Audio(data=processed_audio, rate=sample_rate))
            
# # # Swap keys and values
# emotion = {
#     '03' : 'happy',
#     '01' : 'neutral',
#     '02' : 'neutral',
#     '04' : 'sad',
#     '05' : 'angry',
#     '06' : 'fearful',
#     '07' : 'disgust',
#     '08' : 'surprised'
# }
# swapped_emotion_dic = {v: k for k, v in emotion.items()}
# def encode(label):
#     return int(swapped_emotion_dic.get(label))

# # Function to pitch-shift the audio
# def pitch(data, sr, n_steps=2.0):
#     pitched_audio = librosa.effects.pitch_shift(y=data, sr=sr, n_steps=n_steps)
#     return pitched_audio

# # Function to add additive white Gaussian noise to the audio (AWGN)
# def noise(data, noise_level=0.005):
#     noise = np.random.normal(0, noise_level, len(data))
#     return data + noise

# # Function to get features from the audio file
# def get_features(path, duration=2.5, offset=0.6, noise_level=0.005):
#     data, sr = librosa.load(path, duration=duration, offset=offset)
#     aud = extract_features(data)
#     audio = np.array(aud)

#     # Original audio
#     noised_audio = noise(data, noise_level=noise_level)
#     aud2 = extract_features(noised_audio)
#     audio = np.vstack((audio, aud2))  # Add features of noised audio to the result

#     # Pitch-shifted audio
#     pitched_audio = pitch(data, sr)
#     aud3 = extract_features(pitched_audio)
#     audio = np.vstack((audio, aud3))  # Add features of pitch-shifted audio to the result

#     # Pitch-shifted and noised audio
#     pitched_audio1 = pitch(data, sr)
#     pitched_noised_audio = noise(pitched_audio1, noise_level=noise_level)
#     aud4 = extract_features(pitched_noised_audio)
#     audio = np.vstack((audio, aud4))  # Add features of pitch-shifted and noised audio to the result

#     return audio

# # Function to extract MFCC features
# # Capture the spectral characteristics of the audio signal
# def mfcc(data, sr, frame_length=2048, hop_length=512, flatten=True):
#     mfcc_result = librosa.feature.mfcc(y=data, sr=sr)
#     return np.ravel(mfcc_result) if flatten else mfcc_result

# # Function to calculate Spectral Centroid
# # Indicates the "center of mass" of the spectrum, providing information about the brightness of the sound
# def spectral_centroid(data, sr, frame_length=2048, hop_length=512):
#     spectral_centroid_result = librosa.feature.spectral_centroid(y=data, sr=sr)
#     return np.ravel(spectral_centroid_result)

# # Function to calculate Spectral Contrast
# # Captures the difference in amplitude between peaks and valleys in the spectrum.
# def spectral_contrast(data, sr, frame_length=2048, hop_length=512):
#     spectral_contrast_result = librosa.feature.spectral_contrast(y=data, sr=sr)
#     return np.ravel(spectral_contrast_result)

# # Function to extract various audio features
# def extract_features(data, sr=22050, frame_length=2048, hop_length=512):
#     result = []

#     # MFCC feature extraction
#     mfcc_features = mfcc(data, sr, frame_length, hop_length)
#     if mfcc_features.size > 0:
#         result.append(mfcc_features)

#     # Spectral Centroid feature extraction
#     spectral_centroid_features = spectral_centroid(data, sr, frame_length, hop_length)
#     if spectral_centroid_features.size > 0:
#         result.append(spectral_centroid_features)

#     # Spectral Contrast feature extraction
#     spectral_contrast_features = spectral_contrast(data, sr, frame_length, hop_length)
#     if spectral_contrast_features.size > 0:
#         result.append(spectral_contrast_features)

#     # Concatenate feature vectors
#     if len(result) > 0:
#         return np.hstack(result)
#     else:
#         return np.array([])
    
# import timeit
# import warnings
# warnings.filterwarnings('ignore')
# from joblib import Parallel, delayed
# # from tqdm import tqdm
# start = timeit.default_timer()

# def process_feature(path):
#     features = get_features(path)
#     print(features)
#     X = []
#     Y = []
#     for ele in features:
#         X.append(ele)
#         # appending emotion 3 times as we have made 3 augmentation techniques on each audio file.
#         # Y.append(emotion)
#     return X
    
# model = load_model('audio_cnn_lstm_model.h5')
# print("Audio classification Model loaded!")
        
        
        # filename = file.filename

        # # Save the uploaded audio file temporarily
        # file_path = os.path.join('audio_uploads', filename)
        # file.save(file_path)
        
        # #1 preproces
        # # x, sr = preprocess_audio_file(file_path)
        # # print('preprocessing done!')
        # #2 swap keys
        
        # #3 feature ext
        # # print(get_features(file_path))
        # #4 pitch shifting
        # # X = process_feature(file_path)
        # X = np.array([get_features(file_path)])
        # print(X)
        # print(model.summary())
        # X_audio = np.expand_dims(X, axis=0)  # Add batch size dimension
        # X_audio = np.expand_dims(X_audio, axis=3) # Add channels (1)
        # # # Reshape features for CNN+LSTM input (1 sample, time steps, feature dimension)
        # # features = np.expand_dims(features, axis=0)
        # # features = np.expand_dims(features, axis=3)  # Add channel dimension if required by the model
        # ratings_input = np.array([[0]])
        # prediction = model.predict([X, ratings_input])
        # print(prediction)
        # # emotion_label = np.argmax(prediction, axis=1)
        # # # Predict emotion using the model
        # # prediction = model.predict(features)
        # # emotion_label = np.argmax(prediction, axis=1)  # Get the predicted emotion label

        # classification_result = f"🎧 Emotion for {file.filename}: {emotion_label}"
        
        
        # classification_result = f"🤢 Emotion for {file.filename}" 